#include <iostream>
using namespace std;
int main()
{
    int jour;
    int mois;
    int annee;
    char retry;

    cout << "Saisir le jour : " << endl;
    cin >> jour;
    cout << "Saisir le mois : " << endl;
    cin >> mois;
    cout << "Saisir l'annee : " << endl;
    cin >> annee;

    if (jour > 31 || jour < 1 || mois>12 || mois < 1 || annee>2020 || annee < -1000)
    {
        cout << "error" << endl;
    }

    else 
    {
        if (jour == 31 && mois != 12) {
            jour = 1;
            mois++;
            cout << "La date du lendemain est : " << jour << "/" << mois << "/" << annee;
        }
        else if (mois == 4 || mois == 6 || mois == 9 || mois == 11)
        {
            jour = 1;
            mois++;
            cout << "La date du lendemain est : " << jour << "/" << mois << "/" << annee;
        }
        else if (jour == 31 && mois == 12)
        {
            jour = 1;
            mois = 1;
            annee++;
            cout << "La date du lendemain est : " << jour << "/" << mois << "/" << annee;
        }
        else {
            jour++;
            cout << "La date du lendemain est : " << jour << "/" << mois << "/" << annee;
        }
    }
}
